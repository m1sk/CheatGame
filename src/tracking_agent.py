from cheat_game_server import Player, Claim, Take_Card, Call_Cheat, ActionEnum, Cheat


class GameState:
    def __init__(self):
        self.in_hand = []
        self.played = []
        # TODO keep track of cards that the opponent picks up,
        #  for example if he cheats and is caught then he has all of my cards
        # self.cards_opponent_claimed = []
        self.count_opponent = 0
        self.count_table = 0
        self.last_claim = None
        self.last_action = None
        # self.count_deck = 0
        pass


class TrackingAgent(Player):
    def __init__(self, name, silent=False):
        super(TrackingAgent, self).__init__(name)
        self.silent = silent

        self.state = GameState()
        self.state.in_hand = self.cards

    def connect_to_game(self, game):
        super(TrackingAgent, self).connect_to_game(game)
        if game is not None:
            self.state.last_claim = game.last_claim()

    def make_claim(self, cards, claim):
        if not self.silent: print 'making claim: {0:1d} cards of rank {1}'.format(claim.count, str(claim.rank))
        self.state.played.extend(cards)
        super(TrackingAgent, self).make_claim(cards, claim)

    def make_honest_claim(self, claim):
        super(TrackingAgent, self).make_honest_claim(claim)

    def take_card_from_deck(self,silent=False):
        if not (self.silent or silent): print 'Taking Card from deck'
        super(TrackingAgent, self).take_card_from_deck()

    def take_cards_from_table(self):
        """
        Take all the cards from the table\n
        Called by loser of "Call Cheat" event\n
        :return: cards
        """
        cards = self.table.take_cards()
        if not self.silent:
            print 'Player {0} "{1}" received the cards from the table'.format(self.id, self.name)
            print '  Cards: ', ' '.join([str(card) for card in cards])
            print '         ', ' '.join([str(card.suit) for card in cards])
        self.cards.extend(cards)
        return cards

    def call_cheat(self):
        if not self.silent: print 'Calling "Cheat!"'
        self.state.played = []
        super(TrackingAgent, self).call_cheat()

    def get_state(self):
        self.state.last_claim = self.table.last_claim()
        return self.state

    def make_move(self):
        if not self.silent:
            print
            print 'Player {0:1d} ({1:s}) turn'.format(self.id, self.name)
            print "================"+"="*len(self.name)
        honest_moves = self.possible_honest_moves()
        state = self.game.get_state()
        opponent_count = state[3 - self.id]
        deck_count = state['DECK']
        table_count = state['TABLE']
        last_action = state['LAST_ACTION']
        cards_revealed = state['CARDS_REVEALED']
        last_claim = self.game.last_claim()

        if last_action == ActionEnum.CALL_CHEAT:
            self.state.played = []
        elif last_action == ActionEnum.MAKE_CLAIM:
            self.state.last_claim = last_claim
        self.state.count_opponent = opponent_count
        self.state.count_table = table_count
        self.state.last_action = last_action
        # if opponent placed his last cards on the table - call_cheat or lose
        action = self.agent_logic(deck_count, table_count, opponent_count,
                                  last_action, last_claim, honest_moves, cards_revealed)
        if isinstance(action, Call_Cheat):
            self.call_cheat()
        elif isinstance(action, Claim):
            self.make_honest_claim(action)
        elif isinstance(action, Take_Card):
            self.take_card_from_deck()
        elif isinstance(action, Cheat):
            self.make_claim(action.cards, Claim(action.rank, action.count))
