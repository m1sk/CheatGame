import numpy as np



class MonteCarlo:
    def __init__(self, game, state,num_samples=100,silent=True):
        self.num_samples = num_samples
        self.game = game
        self.game_state = state
        self.silent = silent
        pass

    def simulate(self, move):
        wins = 0.0
        losses = 0.0
        for i in range(self.num_samples):
            if not self.silent: print "starting to play game %d" % i
            if self.simulate_once(move):
                wins += 1
            else:
                losses += 1
            if not self.silent: print "finished game %d, currently winning %d/%d" % (i,wins,wins+losses)
        return (wins - losses) / (wins + losses)

    def simulate_once(self, move):
        self.game.set_state(self.game_state)
        self.game.play_with_first_move(move)
        return self.game.won()

    def best_move(self, moves):
        best_move = None
        best_move_value = -10
        for move in moves:
            value = self.simulate(move)
            if value > best_move_value:
                best_move = move
                best_move_value = value
        return best_move

    def best_move_using_bandit(self, moves):
        best_move = None
        best_move_value = -10
        bandit = EpsilonGreedy(len(moves))
        for _ in range(self.num_samples):
            i = bandit.choose_arm()
            move = moves[i]
            value = 1 if self.simulate_once(move) else -1
            bandit.update(i,value)
        return moves[bandit.choose_arm()]


class EpsilonGreedy(object):
    def __init__(self,n_arms,epsilon_decay=50):
        self.n = n_arms
        self.counts = [0] * self.n  # example: number of views
        self.values = [0.] * self.n # example: number of clicks / views
        self.decay = epsilon_decay

    def choose_arm(self):
        """Choose an arm for testing"""
        epsilon = self.get_epsilon()
        if np.random.random() > epsilon:
            # Exploit (use best arm)
            return self.get_best_arm()
        else:
            # Explore (test all arms)
            return np.random.randint(self.n)

    def get_best_arm(self):
        return np.argmax(self.values)

    def update(self,arm,reward):
        """Update an arm with some reward value""" # Example: click = 1; no click = 0
        self.counts[arm] += 1
        n = self.counts[arm]
        value = self.values[arm]
        # Running product
        new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
        self.values[arm] = new_value

    def get_epsilon(self):
        """Produce epsilon"""
        total = np.sum(self.counts)
        return float(self.decay) / (total + float(self.decay))