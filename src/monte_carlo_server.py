import numpy as np

from cheat_game_server import Game, Card, Rank, Suit, ActionEnum, Claim, Cheat, \
    Call_Cheat, Take_Card, Deck, Table
from monte_carlo_algorithm import MonteCarlo
from monte_carlo_dummy import MonteCarloDemoAgent
from tracking_agent import GameState


class MonteCarloCheatServer(Game, MonteCarlo):
    def __init__(self, state, player1=MonteCarloDemoAgent('player'), player2=MonteCarloDemoAgent('opponent'), num_of_samples=10,with_jokers=False, lose_on_30=False):
        """
        assume that the first player is the current player
        :param player1: first player  (derived from abstract class Player)
        :param player2: second player (derived from abstract class Player)
        :param with_jokers: optional flag. when true - add two jokers to deck
        :param lose_on_30:  optional flag. when true - game ends when a player has at least 30 cards
        :return: an initialized Game object
        """
        Game.__init__(self, player1, player2, with_jokers, lose_on_30)
        MonteCarlo.__init__(self,self,state,num_of_samples)
        self.players = self._Game__players
        # self.set_state(player1, player2, state)

    def set_state(self, state):
        self.state = []
        player1 = self.players[0]
        player2 = self.players[1]
        self.return_all_cards()
        for card in state.played:
            self.deck._cards.remove(card)
            self.table. _cards.append(card)
        for card in state.in_hand:
            self.deck._cards.remove(card)
        player1.cards = state.in_hand[:]

        self.last_action = state.last_action
        claimed_cards = []
        if state.last_claim:
            self.table.claims = [state.last_claim]
            if isinstance(self.last_action,Claim) and np.random.random() > 0.5:
                #pick the last cards for the table
                #at least 50% of the time should match the claim
                # dont forget to remove them from the deck!
                #use this:
                try:
                    for _ in range(self.last_action.count):
                        claimed_cards.append(self.deck.deal_specific(self.last_action.rank))
                except AssertionError:
                    pass
        else:
            self.table.claims = []
        self.table.initial_dummy_claim = state.last_claim

        for _ in range(state.count_table - len(state.played) - len(claimed_cards)):
            card = self.deck.deal_top()
            self.table._cards.append(card)
        #must be last because they are the claimed
        self.table._cards.extend(claimed_cards)
        for _ in range(state.count_opponent):
            player2.take_card_from_deck(silent=True)


    def return_all_cards(self):
        self.deck = Deck(jokers=self.with_jokers)
        self.table = Table()

        for player in self.players:
            player.cards = []
            player.connect_to_game(self)
            player.connect_to_game(self)


    @staticmethod
    def perform_move(player, move):
        '''
        Code copied from Agent.make_move
        :param player:
        :param move:
        :return:
        '''
        if isinstance(move, Call_Cheat):
            player.call_cheat()
        elif isinstance(move, Claim):
            player.make_honest_claim(move)
        elif isinstance(move, Take_Card):
            player.take_card_from_deck()
        elif isinstance(move, Cheat):
            player.make_claim(move.cards, Claim(move.rank, move.count))

    def play_with_first_move(self,first_move):
        """
        main game loop\n
        this is all copied from original game 'play' method
        the only difference is the fact that the first player is always the first player
        and that you must provide a first move
        :return: None
        """
        # randomly choose first player
        self.cur_player = 0
        call_cheat_timeout = 0
        is_first_move = True
        while not self.end_of_game():
            current_player = self.players[self.cur_player]
            current_player.sort_cards()
            self.update_cheat_flag()
            prev_claim = None
            if self.table.claims:
                prev_claim = self.table.last_claim()
            deck_num_cards = len(self.deck._cards)
            table_num_claims = len(self.table.claims)

            if is_first_move:
                MonteCarloCheatServer.perform_move(current_player, first_move)
                is_first_move = False
            else:
                current_player.make_move()

            if len(self.deck._cards) < deck_num_cards:
                self.last_action = ActionEnum.TAKE_CARD
            elif len(self.table.claims) > table_num_claims:
                self.last_action = ActionEnum.MAKE_CLAIM
            else:
                self.last_action = ActionEnum.CALL_CHEAT
                call_cheat_timeout = 2
            if call_cheat_timeout > 0:
                call_cheat_timeout -= 1
            else:
                self.cards_revealed = None
            if self.table.claims:
                self.no_new_claim = (prev_claim == self.table.last_claim())
            self.save_state()
            self.cur_player = 1 - self.cur_player

    def won(self):
        return self.winner == self.players[0]

if __name__ == '__main__':
    state = GameState()
    nine_of_clubs = Card(Rank.NINE, Suit.CLUB)
    five_of_clubs = Card(Rank.FIVE, Suit.CLUB)
    state.in_hand = [nine_of_clubs, five_of_clubs]
    state.played = [Card(Rank.EIGHT, Suit.HEART), Card(Rank.TEN, Suit.HEART),]
    state.count_opponent = 0
    state.count_table = 10
    state.last_claim = Claim(Rank.FOUR, 1)

    monte_carlo = MonteCarloCheatServer(state, num_of_samples=20)
    # monte_carlo.silent = False
    # print monte_carlo.simulate(Cheat([nine_of_clubs],Rank.FIVE,1))
    print monte_carlo.simulate(Claim(Rank.FIVE,1))
    print monte_carlo.simulate(Take_Card())
    print monte_carlo.simulate(Call_Cheat())
    # print monte_carlo.best_move([Cheat([nine_of_clubs],Rank.FIVE,1),Claim(Rank.FIVE,1)])


