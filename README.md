#CheatGame
Assignment for advanced AI course.
Involves creating an AI for the cheat card game


#Notes
We want implement this with a monte carlo simulation

This will require 3 separate components:
1. A custom cheat game server to run the monte carlo simulation.
this server should support playing from the current position.
and randomly distribute the cards that are definitely played between
the deck, the cards the opponent played and the opponent's cards.
my current cards should stay and be played by the agents.
2. A dummy game agent to play the monte carlo simulations.
the agent should recognize states where the right move is obvious.
But otherwise should play entirely at random.
Must be initialized with the current state.
3. Create a monte carlo agent that runs this server to find the best move.
When the move is obvious - just do it, otherwise create some servers.

